<?php

// TCMB API'si için gerekli bilgiler
$api_url = 'https://evds2.tcmb.gov.tr/service/evds/';
$api_method = 'series';
$api_key = '<TCMB API Anahtarı>';

// API'den çekilecek verilerin ayarlanması
$series_id = 'TP.DK.USD.A';
$start_date = date('d.m.Y', strtotime('-1 day')); // Dünkü tarihin formatı
$end_date = date('d.m.Y');

// API isteği için gereken URL'nin oluşturulması
$url = $api_url . $api_method . '?'
     . 'series_id=' . $series_id
     . '&start_date=' . $start_date
     . '&end_date=' . $end_date
     . '&apikey=' . $api_key;

// API'den verilerin çekilmesi
$response = file_get_contents($url);
$data = json_decode($response, true);

// Son dolar kuru değerinin alınması
$usd_rate = end($data['items'])['value'];

echo 'Dolar kuru: ' . $usd_rate;